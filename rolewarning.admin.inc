<?php

/**
 * Configuration form to select which users are to be considered dangerous
 */
function rolewarning_admin_form() {
  //get all defined roles in the system, leaving out the anonymous role.
  $roles = user_roles(TRUE);

  $form['rolewarning_dangerous_roles'] = array(
    '#title' => t('Select dangerous roles'),
    '#description' => t('Select which roles should be considered dangerous and should generate a warning when editing or creating a new user.'),
    '#type' => 'checkboxes',
    '#options' => $roles,
    '#default_value' => variable_get('rolewarning_dangerous_roles', NULL)
  );

  return system_settings_form($form);
}
