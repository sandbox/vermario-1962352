(function ($) {
  Drupal.behaviors.us_rolewarning = {
    attach: function (context, settings) {

      $('form#user-profile-form, form#user-register-form', context).submit(function (e) {
        e.preventDefault();
        var form = this;
        var checkdanger = us_rolewarning_checkdangerous();
        var proceed = true;
        var confirmtext = Drupal.t('This user will be associated with dangerous roles. Are you sure you want to continue?');

        if (checkdanger) {
          proceed = confirm(confirmtext);
        }
        if (proceed) {
          form.submit();
        }
      });

      var obj=Drupal.settings.rolewarning.dangerousRoles;
      //add visible warning to roles checkboxes
      $.each( obj, function(i, n){
        $('#edit-roles .form-item-roles-'+n, context).addClass('rolewarning-dangerous');
      });
    }
  };

  //this checks if the user role checkboxes contain sensitive roles.
  function us_rolewarning_checkdangerous() {
    var danger = false;
    var obj=Drupal.settings.rolewarning.dangerousRoles;

    $('#edit-roles :checked').each(function () {
      for (var k in obj) {
        if (!obj.hasOwnProperty(k)) continue;
        if (obj[k] === $(this).val()) {
          danger = true;
        }
      }
    });
    return danger;
  }
})(jQuery);
